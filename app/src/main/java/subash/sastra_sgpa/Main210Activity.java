package subash.sastra_sgpa;



        import android.support.v7.app.ActionBarActivity;
        import android.os.Bundle;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.TextView;


public class Main210Activity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main210);
    }
    String g[] = new String[25];
    String w[] = new String[25];
    int m[] = new int[25];
    int c[] = new int[25];
    int tot[] = new int[25];
    int i;

    public void onClick10(View v1) {
        double ctot = 0.0, mtot = 0.0, sg = 0, flag = 0, flag1 = 0;
        int temp =0;
        Button b1 = (Button) v1;
        EditText tg1 = (EditText) findViewById(R.id.tg1);
        EditText tg2 = (EditText) findViewById(R.id.tg2);
        EditText tg3 = (EditText) findViewById(R.id.tg3);
        EditText tg4 = (EditText) findViewById(R.id.tg4);
        EditText tg5 = (EditText) findViewById(R.id.tg5);
        EditText tg6 = (EditText) findViewById(R.id.tg6);
        EditText tg7 = (EditText) findViewById(R.id.tg7);
        EditText tg8 = (EditText) findViewById(R.id.tg8);
        EditText tg9 = (EditText) findViewById(R.id.tg9);
        EditText tg10 = (EditText) findViewById(R.id.tg10);
        EditText tg11 = (EditText) findViewById(R.id.tg11);
        EditText tg12 = (EditText) findViewById(R.id.tg12);
        EditText tg13 = (EditText) findViewById(R.id.tg13);
        EditText tc1 = (EditText) findViewById(R.id.tc1);
        EditText tc2 = (EditText) findViewById(R.id.tc2);
        EditText tc3 = (EditText) findViewById(R.id.tc3);
        EditText tc4 = (EditText) findViewById(R.id.tc4);
        EditText tc5 = (EditText) findViewById(R.id.tc5);
        EditText tc6 = (EditText) findViewById(R.id.tc6);
        EditText tc7 = (EditText) findViewById(R.id.tc7);
        EditText tc8 = (EditText) findViewById(R.id.tc8);
        EditText tc9 = (EditText) findViewById(R.id.tc9);
        EditText tc10 = (EditText) findViewById(R.id.tc10);
        EditText tc11 = (EditText) findViewById(R.id.tc11);
        EditText tc12 = (EditText) findViewById(R.id.tc12);
        EditText tc13 = (EditText) findViewById(R.id.tc13);
        TextView tsg = (TextView) findViewById(R.id.tsg);
        g[0] = tg1.getText().toString();
        g[1] = tg2.getText().toString();
        g[2] = tg3.getText().toString();
        g[3] = tg4.getText().toString();
        g[4] = tg5.getText().toString();
        g[5] = tg6.getText().toString();
        g[6] = tg7.getText().toString();
        g[7] = tg8.getText().toString();
        g[8] = tg9.getText().toString();
        g[9] = tg10.getText().toString();
        g[10] = tg11.getText().toString();
        g[11] = tg12.getText().toString();
        g[12] = tg13.getText().toString();
        c[0] = Integer.parseInt(tc1.getText().toString());
        c[1] = Integer.parseInt(tc2.getText().toString());
        c[2] = Integer.parseInt(tc3.getText().toString());
        c[3] = Integer.parseInt(tc4.getText().toString());
        c[4] = Integer.parseInt(tc5.getText().toString());
        c[5] = Integer.parseInt(tc6.getText().toString());
        c[6] = Integer.parseInt(tc7.getText().toString());
        c[7] = Integer.parseInt(tc8.getText().toString());
        c[8] = Integer.parseInt(tc9.getText().toString());
        c[9] = Integer.parseInt(tc10.getText().toString());
        c[10] = Integer.parseInt(tc11.getText().toString());
        c[11] = Integer.parseInt(tc12.getText().toString());
        c[12] = Integer.parseInt(tc13.getText().toString());
        for (i = 0; i < 13; i++) {
            if (c[i] == 1 || c[i] == 2 || c[i] == 3 || c[i] == 4 || c[i] == 5 || c[i] == 6 || c[i] == 7 || c[i] == 8 || c[i] == 9) {
                flag1++;
            }
        }

        for (i = 0; i < 13; i++) {
            switch (g[i]) {
                case "s":
                case "S":
                case " s":
                case " S":
                    m[i] = 10;
                    flag++;
                    break;
                case "a+":
                case "A+":
                    m[i] = 9;
                    flag++;
                    break;

                case "a":
                case "A":
                case " a":
                case " A":
                    m[i] = 8;
                    flag++;
                    break;
                case "b":
                case "B":
                case " b":
                case " B":
                    m[i] = 7;
                    flag++;
                    break;
                case "c":
                case "C":
                case " c":
                case " C":
                    m[i] = 6;
                    flag++;
                    break;
                case "d":
                case "D":
                case " d":
                case " D":
                    m[i] = 5;
                    flag++;
                    break;
                case "f":
                case "F":
                case " f":
                case " F":
                    m[i] = 2;
                    flag++;
                    break;
                case "e":
                case "E":
                case " e":
                case " E":
                    m[i] = 2;
                    flag++;
                    break;
            }
        }


        if (flag == 13 && flag1==13) {
            for (i = 0; i < 13; i++) {
                tot[i] = m[i] * c[i];
                mtot += tot[i];
                ctot += c[i];
            }

            sg = mtot / ctot;
            tsg.setText(String.valueOf(sg));
        } else {
            tsg.setText(String.valueOf(sg));
        }
    }

}
